FROM node:latest
COPY . .
WORKDIR .
EXPOSE 3000
ENTRYPOINT node index.js
