const http = require('http');
const Redis = require('ioredis');

const server = http.createServer(function(req,res){
    let redis = new Redis();
    redis.get('counter').then(function(result){
        if(result !== undefined){
            result = Number.parseInt(result) + 1;            
        }
        else{
            result = 1;
        }

        redis.set('counter',result);
        res.write('Access count : ' + result.toString());
        res.end();
    });    
});
server.listen(3000);
